import React, { useState } from 'react'
import { Form, InputGroup, FormControl, Button } from 'react-bootstrap'
import Select from 'react-select'
import DatePicker from 'react-date-picker'
import { useHistory } from 'react-router-dom'
import 'react-datepicker/dist/react-datepicker.css'

const options = [
  { value: 'pune', label: 'Pune' },
  { value: 'buldhana', label: 'Buldhana' },
  { value: 'mumbai', label: 'Mumbai' },
  { value: 'nashik', label: 'Nashik' },
]
const UserInfo = (props) => {
  let history = useHistory()
  const {
    location: { GlobalState },
  } = props


  let UserState = {}


  const [globalState, setGlobalState] = useState({
    firstName: null,
    lastName: null,
    email: null,
    mobile: null,
    dateOfBirth: null,
    city: null,
  })

  const { firstName, lastName, mobile, email, dateOfBirth, city } =
    UserState && UserState.firstName ? UserState : globalState
  if (
    GlobalState &&
    GlobalState.UserState &&
    GlobalState.UserState.firstName &&
    !firstName
  ) {
    UserState = GlobalState.UserState
    setGlobalState(UserState)
  }

  const [isMobileValid, setIsMobileInvalid] = useState(false)
  const [isEmailValid, setIsEmailInvalid] = useState(false)

  const updateState = (value, key) => {
    if (key === 'mobile') {
      let MobilePattern = /^[7-9][0-9]{9}$/
      if (value && value.match(MobilePattern)) {
        setIsMobileInvalid(true)
      } else {
        setIsMobileInvalid(false)
      }
    }
    if (key === 'email') {
      let EmailPattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      console.log('email validation', value.match(EmailPattern))
      if (value && value.match(EmailPattern)) {
        setIsEmailInvalid(true)
      } else {
        setIsEmailInvalid(false)
      }
    }
    setGlobalState({ ...globalState, [key]: value })
  }

  const handleChangeCity = (city) => {
    setGlobalState({ ...globalState, city })
  }

  const handleChangeDOB = (dateOfBirth) => {
    dateOfBirth = new Date(dateOfBirth)
    setGlobalState({ ...globalState, dateOfBirth })
  }

  const checkMobileNumberValid = (mobile) => {
    let MobilePattern = /^[7-9][0-9]{9}$/
    if (mobile && !mobile.match(MobilePattern)) {
      return false
    }
    return true
  }

  const checkEmailValid = (email) => {
    let EmailPattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if (email && !email.match(EmailPattern)) {
      return false
    }
    return true
  }

  const checkIfValid = () => {
    if (firstName && lastName && email && mobile && dateOfBirth && city) {
      if (checkMobileNumberValid(mobile) && checkEmailValid(email)) {
        history.push({ pathname: '/career', GlobalState: {
          CareerState: GlobalState && GlobalState.CareerState && GlobalState.CareerState.companyName ? GlobalState.CareerState : {},
          UserState:globalState,
        }
       })
      }
    }
  }

  return (
    <Form>
      <h2 style={{ textAlign: 'center' }}>User Info</h2>
      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text id="basic-addon1">First Name</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          defaultValue={firstName}
          onChange={(e) => updateState(e.target.value, 'firstName')}
          placeholder="First Name"
          aria-label="First Name"
          aria-describedby="basic-addon1"
        />
      </InputGroup>
      {!firstName ? (
        <div style={{ color: 'red' }}>Please enter first name</div>
      ) : (
        []
      )}
      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text id="basic-addon1">Last Name</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          onChange={(e) => updateState(e.target.value, 'lastName')}
          defaultValue={lastName}
          placeholder="Last Name"
          //react-bootstrap
          aria-label="Last Name"
          aria-describedby="basic-addon1"
        />
      </InputGroup>
      {!lastName ? (
        <div style={{ color: 'red' }}>Please enter last name</div>
      ) : (
        []
      )}

      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text id="basic-addon1">Mobile</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          onChange={(e) => updateState(e.target.value, 'mobile')}
          placeholder="Mobile"
          defaultValue={mobile}
          aria-label="Mobile"
          aria-describedby="basic-addon1"
        />
      </InputGroup>
      {!mobile ? (
        <div style={{ color: 'red' }}>Please enter mobile</div>
      ) : !isMobileValid ? (
        <div style={{ color: 'red' }}>mobile number is not valid</div>
      ) : (
        []
      )}

      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text id="basic-addon1">Email</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          defaultValue={email}
          onChange={(e) => updateState(e.target.value, 'email')}
          placeholder="Email"
          aria-label="Email"
          aria-describedby="basic-addon1"
        />
      </InputGroup>
      {!email ? (
        <div style={{ color: 'red' }}>Please enter email</div>
      ) : !isEmailValid ? (
        <div style={{ color: 'red' }}>email is not valid</div>
      ) : (
        []
      )}
      <Form.Group controlId="formGroupEmail">
        <Form.Label>Choose city</Form.Label>
        <Select
          value={city}
          onChange={handleChangeCity}
          options={options}
          placeholder={'Select your city'}
        />
      </Form.Group>
      {!city ? <div style={{ color: 'red' }}>Please enter city</div> : []}
      <Form.Group controlId="formGroupPassword">
        <div>
          <Form.Label>Date of birth</Form.Label>
        </div>
        <DatePicker onChange={handleChangeDOB} value={dateOfBirth} />
      </Form.Group>
      {!dateOfBirth ? (
        <div style={{ color: 'red' }}>Please enter date of birth</div>
      ) : (
        []
      )}
      <p>
        <Button onClick={checkIfValid} style={{ float: 'right' }}>
          Next Step
        </Button>
      </p>
    </Form>
  )
}

export default UserInfo
