import React from 'react'
import { Form, InputGroup, FormControl, Button } from 'react-bootstrap'
import Select from 'react-select'
import DatePicker from 'react-date-picker'
import { useHistory } from 'react-router-dom'
import ContinuousSlider from '../Elements/ContinuousSlider'
import Swal from 'sweetalert2'

const options = [
  { value: 'pune', label: 'Pune' },
  { value: 'buldhana', label: 'Buldhana' },
  { value: 'mumbai', label: 'Mumbai' },
  { value: 'nashik', label: 'Nashik' },
]

export default function Summary(props) {
  let history = useHistory()

  const onError = () => {
    Swal.fire({
      title: 'Error!',
      text: 'Do you want to continue',
      icon: 'error',
      confirmButtonText: 'Cool',
    })
  }

  const onSucess = () => {
    Swal.fire({
      title: 'Success!',
      text: ' ',
      icon: 'success',
      confirmButtonText: 'OK',
    })
  }

  let {
    location: { GlobalState },
  } = props

  if (!GlobalState) {
    GlobalState = {
      CareerState: {
        companyName: '',
        jobTitle: '',
        socialProfile1: '',
        socialProfile2: '',
        reactExpertise: '',
        summary: '',
      },
      UserState: {
        firstName: '',
        lastName: '',
        mobile: '',
        email: '',
        dateOfBirth: '',
        city: '',
      },
    }
  }
  const { CareerState, UserState } = GlobalState
  const {
    companyName,
    jobTitle,
    socialProfile1,
    socialProfile2,
    reactExpertise,
    summary,
  } = CareerState

  const { firstName, lastName, mobile, email, dateOfBirth, city } = UserState

  const handleChangeExpertise = () => {}

  const handleSubmit = () => {}

  const goBack = () => {
    history.push({
      pathname: '/career',
      GlobalState: {
        CareerState: GlobalState.CareerState,
        UserState:GlobalState.UserState,
      },
    })
  }
  return (
    <div>
      <h2 style={{ textAlign: 'center' }}>Summary page</h2>
      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text id="basic-addon1">First Name</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          value={firstName}
          disabled={true}
          placeholder="First Name"
          aria-label="First Name"
          aria-describedby="basic-addon1"
        />
      </InputGroup>
      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text id="basic-addon1">Last Name</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          value={lastName}
          disabled={true}
          placeholder="Last Name"
          aria-label="Last Name"
          aria-describedby="basic-addon1"
        />
      </InputGroup>
      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text id="basic-addon1">Mobile</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          value={mobile}
          disabled={true}
          placeholder="Mobile"
          aria-label="Mobile"
          aria-describedby="basic-addon1"
        />
      </InputGroup>
      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text id="basic-addon1">Email</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          value={email}
          disabled={true}
          placeholder="Email"
          aria-label="Email"
          aria-describedby="basic-addon1"
        />
      </InputGroup>
      <Form>
        <Form.Group controlId="formGroupEmail">
          <Form.Label>Choose city</Form.Label>
          <Select
            disabled={true}
            value={city}
            options={options}
            placeholder={'Select your city'}
          />
        </Form.Group>
        <Form.Group controlId="formGroupPassword">
          <div>
            <Form.Label>Date of birth</Form.Label>
          </div>
          <DatePicker value={dateOfBirth} disabled={true} />
        </Form.Group>
      </Form>
      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text id="basic-addon1">Company Name</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          value={companyName}
          disabled={true}
          placeholder="Company Name"
          aria-label="Company Name"
          aria-describedby="basic-addon1"
        />
      </InputGroup>
      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text id="basic-addon1">Job Title</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          disabled={true}
          value={jobTitle}
          placeholder="Job Title"
          aria-label="Job Title"
          aria-describedby="basic-addon1"
        />
      </InputGroup>
      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text id="basic-addon1">Social Profile 1</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          disabled={true}
          value={socialProfile1}
          placeholder="Social Profile 1"
          aria-label="Social Profile 1"
          aria-describedby="basic-addon1"
        />
      </InputGroup>
      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text id="basic-addon1">Social Profile 2</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          value={socialProfile2}
          disabled={true}
          placeholder="Social Profile 2"
          aria-label="Social Profile 2"
          aria-describedby="basic-addon1"
        />
      </InputGroup>
      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text id="basic-addon1">Summary</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          value={summary}
          disabled={true}
          placeholder="Summary"
          aria-label="Summary"
          aria-describedby="basic-addon1"
        />
      </InputGroup>
      <Form>
        <Form.Group controlId="formGroupEmail">
          <Form.Label>
            Expertise in React{' '}
            {reactExpertise > 70 ? '(Excellent)' : '(Moderate)'}
          </Form.Label>
          <ContinuousSlider
            reactExpertise={reactExpertise}
            handleChangeExpertise={handleChangeExpertise}
          />
        </Form.Group>
      </Form>
      <p>
        <Button
          style={{ float: 'right', marginTop: '16px' }}
          onClick={onSucess}
        >
          Submit
        </Button>
      </p>
      <p>
        <Button
          onClick={goBack}
          style={{ float: 'right', marginRight: '10px' }}
        >
          Previous Step
        </Button>
      </p>
    </div>
  )
}
