import React, { useState } from 'react'
import { Form, InputGroup, FormControl, Button } from 'react-bootstrap'
import { Link, useHistory } from 'react-router-dom'
import ContinuousSlider from '../Elements/ContinuousSlider'

const CareerInfo = (props) => {
  let history = useHistory()
  const {
    location: { GlobalState },
  } = props

  let CareerState = {}


  const [globalState, setGlobalState] = useState({
    companyName: null,
    jobTitle: null,
    socialProfile1: null,
    socialProfile2: null,
    reactExpertise: 0,
    summary: null,
  })

  const {
    companyName,
    jobTitle,
    socialProfile1,
    socialProfile2,
    reactExpertise,
    summary,
  } = CareerState && CareerState.companyName ? CareerState : globalState
  if (
    GlobalState &&
    GlobalState.CareerState &&
    GlobalState.CareerState.companyName &&
    !companyName
  ) {
    CareerState = GlobalState.CareerState
    console.log("CareerState",CareerState)
    setGlobalState(CareerState)
   
  }
  
  const updateState = (value, key) => {
    setGlobalState({ ...globalState, [key]: value })
  }

  const handleChangeExpertise = (event, newValue) => {
    setGlobalState({ ...globalState, reactExpertise: newValue })
  }

  const checkIfValid = () => {
    if (
      companyName &&
      jobTitle &&
      socialProfile1 &&
      socialProfile2 &&
      reactExpertise &&
      summary
    ) {
      history.push({
        pathname: '/summary',
        GlobalState: {
          CareerState: globalState,
          UserState: GlobalState.UserState &&  GlobalState.UserState.firstName  ? GlobalState.UserState : {} ,
        },
      })
    }
  }

  const goBack = () => {
    history.push({
      pathname: '/',
      GlobalState: {
        CareerState: globalState,
        UserState: GlobalState.UserState &&  GlobalState.UserState.firstName  ? GlobalState.UserState : {},
      },
    })
  }
  return (
    <div>
      <h2 style={{ textAlign: 'center' }}>Career Info</h2>
      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text id="basic-addon1">Company Name</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          onChange={(e) => updateState(e.target.value, 'companyName')}
          defaultValue={companyName}
          placeholder="Company Name"
          aria-label="Company Name"
          aria-describedby="basic-addon1"
        />
      </InputGroup>
      {!companyName ? (
        <div style={{ color: 'red' }}>Please enter company name</div>
      ) : (
        []
      )}
      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text id="basic-addon1">Job Title</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          onChange={(e) => updateState(e.target.value, 'jobTitle')}
          defaultValue={jobTitle}
          placeholder="Job Title"
          aria-label="Job Title"
          aria-describedby="basic-addon1"
        />
      </InputGroup>
      {!jobTitle ? (
        <div style={{ color: 'red' }}>Please enter job title</div>
      ) : (
        []
      )}
      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text id="basic-addon1">Social Profile 1</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          
          onChange={(e) => updateState(e.target.value, 'socialProfile1')}
          defaultValue={socialProfile1}
          placeholder="Social Profile 1"
          aria-label="Social Profile 1"
          aria-describedby="basic-addon1"
        />
      </InputGroup>
      {!socialProfile1 ? (
        <div style={{ color: 'red' }}>Please enter Social Profile</div>
      ) : (
        []
      )}
      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text id="basic-addon1">Social Profile 2</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          onChange={(e) => updateState(e.target.value, 'socialProfile2')}
          defaultValue={socialProfile2}
          placeholder="Social Profile 2"
          aria-label="Social Profile 2"
          aria-describedby="basic-addon1"
        />
      </InputGroup>
      {!socialProfile2 ? (
        <div style={{ color: 'red' }}>Please enter Social Profile</div>
      ) : (
        []
      )}
      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text id="basic-addon1">Summary</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
          onChange={(e) => updateState(e.target.value, 'summary')}
          defaultValue={summary}
          placeholder="Summary"
          aria-label="Summary"
          aria-describedby="basic-addon1"
        />
      </InputGroup>
      {!summary ? <div style={{ color: 'red' }}>Please enter summary</div> : []}
      <Form>
        <Form.Group controlId="formGroupEmail">
          <Form.Label>
            Expertise in React{' '}
            {reactExpertise > 70 ? '(Excellent)' : '(Moderate)'}
          </Form.Label>
          <ContinuousSlider
            reactExpertise={reactExpertise}
            handleChangeExpertise={handleChangeExpertise}
          />
        </Form.Group>
      </Form>
      <p>
        <Button
          onClick={checkIfValid}
          style={{ float: 'right', marginTop: '16px' }}
        >
          Next Step
        </Button>
      </p>
      <p>
        <Button
          onClick={goBack}
          style={{ float: 'right', marginRight: '10px' }}
        >
          Previous Step
        </Button>
      </p>
    </div>
  )
}
export default CareerInfo