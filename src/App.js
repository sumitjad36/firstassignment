import React from 'react'
import UserInfo from './Components/UserInfo'
import CareerInfo from './Components/CareerInfo'
import Summary from './Components/Summary'

import 'bootstrap/dist/css/bootstrap.min.css'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

export default function App() {
  return (
    <div>
      <div style={{ maxWidth: '30rem', margin: '4rem auto' }}>
        <Router>
          <Switch>
            <Route exact path="/" component={UserInfo} />
            <Route path="/career" component={CareerInfo} />
            <Route path="/summary" component={Summary} />
          </Switch>
        </Router>
      </div>
    </div>
  )
}
