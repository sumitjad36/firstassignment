import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Slider from '@material-ui/core/Slider';

const useStyles = makeStyles({
  root: {
    width: 500,
  },
});

export default function ContinuousSlider(props) {
  const classes = useStyles();
  const {reactExpertise ,handleChangeExpertise} = props;

  return ( 
    <div className={classes.root}>
      <Grid container spacing={2}>
        <Grid item>
        </Grid>
        <Grid item xs>
          <Slider value={reactExpertise} onChange={handleChangeExpertise} aria-labelledby="continuous-slider" />
        </Grid>
        <Grid item>
        </Grid>
      </Grid>
    </div>
  );
}
